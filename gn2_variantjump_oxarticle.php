<?php

class GN2_VariantJump_OxArticle extends GN2_VariantJump_OxArticle_Parent
{
    public function getLink( $iLang = null, $blMain = false  )
    {
        $sUrl = parent::getLink($iLang, $blMain);

        // Load Settings
        $data = parse_ini_file(dirname(__FILE__) . '/gn2_variantjump.ini', true);
        $tpl_overwrite = $data['settings']['tpl_overwrite'];
        $mode = intval($data['settings']['mode']);
        $observers = explode(',', $data['settings']['observers']);

        if ($tpl_overwrite) {

            // Load Article
            $parent = $this->getParentArticle();

            if (!is_object($parent)) {

                $redirect = false;
                $variants = $this->getMdSubvariants();

                foreach ($variants as $variant) {

                    if (is_object($variant)) {

                        // Switch Standard Modes
                        switch ($mode) {
                            case "1";
                                // You're the first one. JUMP!
                                $redirect = true;
                                break;

                            case "2";
                                // 0 = green, 1 = yellow, -1 = red
                                $stock = $variant->getStockStatus();

                                // if is not out of stock
                                if($stock >= 0){
                                    $redirect = true;
                                }
                                break;

                            case "3";
                                $isBuyable = $variant->isBuyable();

                                // if is buyable
                                if($isBuyable){
                                    $redirect = true;
                                }
                                break;

                            default:
                                break;
                        }

                        // Special-Functions - Observe lika Boss
                        foreach ($observers as $observer) {
                            $fn = dirname(__FILE__).'/'.$observer.'.php';
                            if ($observer != "" && file_exists($fn)) {
                                include_once $fn;
                                if (class_exists($observer)) {
                                    $object = new $observer;
                                    $redirect = $object->check($variant, $parent, $mode, $redirect);
                                }
                            }
                        }

                        // redirect to this variant if redirecting is enabled
                        if ($redirect) {
                            $sUrl = $variant->getLink();
                            break;
                        }
                    }
                }
            }
        }

        return $sUrl;
    }





}