<?php

/**
 * Module information
 */

 $aModule = array(
    'id'            => 'gn2_variantjump',
    'title'         => 'gn2 :: VariantJump',
    'description'   => '',
    'thumbnail'     => 'gn2.jpg',
    'version'       => '0.1',
    'author'        => 'gn2 netwerk',
    'extend'        => array(
        'oxwarticledetails' => 'gn2netwerk/variantjump/gn2_variantjump_oxwarticledetails',
        'oxarticle'         => 'gn2netwerk/variantjump/gn2_variantjump_oxarticle',
    )
);
